//
//  AppDelegate.swift
//  Budget
//
//  Created by Camilo Gaviria on 17/12/2019.
//  Copyright © 2019 heima. All rights reserved.
//

import CoreData
import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    let coreDataManager = CoreDataManager.shared

    func application(_: UIApplication, didFinishLaunchingWithOptions _: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        coreDataManager.initialize { error in
            if let error = error {
                print("An error ocurred when initializing the Core Data Stack: \(error)")
            }
        }
        return true
    }

    func applicationDidEnterBackground(_: UIApplication) {
        coreDataManager.save()
    }
}
