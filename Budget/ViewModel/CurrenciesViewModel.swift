//
//  CurrenciesViewModel.swift
//  Budget
//
//  Created by Camilo Gaviria on 15/01/2020.
//  Copyright © 2020 heima. All rights reserved.
//

import Foundation

class CurrenciesViewModel {
    private let storageManager = CoreDataManager.shared.storageManager
    private var currencies: [Currency] = []
    private(set) var filteredCurrencies: [Currency] = []

    init() {
        fetch()
    }

    func currency(at indexPath: IndexPath) -> Currency? {
        guard indexPath.row < filteredCurrencies.count else {
            return nil
        }

        return filteredCurrencies[indexPath.row]
    }

    func filter(by searchText: String?) {
        if let searchText = searchText?.lowercased(), !searchText.isEmpty {
            filteredCurrencies = currencies.filter { $0.name?.lowercased().contains(searchText) == true || $0.code?.lowercased().contains(searchText) == true }
        } else {
            filteredCurrencies = currencies
        }
    }

    func fetch() {
        currencies = storageManager.fetchCurrencies()
        filteredCurrencies = currencies
    }
}
