//
//  NewItemViewModel.swift
//  Budget
//
//  Created by Camilo Gaviria on 10/01/2020.
//  Copyright © 2020 heima. All rights reserved.
//

import Base
import CoreData
import ExchangeRates
import Foundation

protocol NewItemViewModelDelegate: AnyObject {
    func convertedValue(_ value: NSNumber?, error: Error?)
}

class NewItemViewModel: BaseItemViewModel {
    private let numberFormatter = NumberFormatter()
    private let currencyFormatter = NumberFormatterFactory.formatter(type: .currency)

    var itemType: ItemType? = .credit
    var itemMode: ItemMode? = .regular {
        didSet {
            let regularFormatter = NumberFormatter()
            regularFormatter.numberStyle = .currency
            regularFormatter.currencySymbol = transferWiseCurrencySymbol
            valueFormatter = itemMode == .regular ? currencyFormatter : regularFormatter
        }
    }

    weak var delegate: NewItemViewModelDelegate?

    var value: NSNumber?
    var convertedValue = NSNumber(value: 0)
    var date: Date = Date()
    var currency: Currency?

    let dateFormatter = DateFormatter()
    var valueFormatter: NumberFormatter!
    var exchangeRatesService: ExchangeRatesService!

    private let transferWiseCurrencySymbol = "$"

    private var currencySymbol: String {
        return valueFormatter.currencySymbol
    }

    var formattedDate: String {
        return dateFormatter.string(from: date)
    }

    var formattedValue: String? {
        guard let price = self.value,
            let formattedValue = valueFormatter.string(from: price) else {
            return ""
        }

        return formattedValue
    }

    var formattedConvertedValue: String {
        guard
            let formattedConvertedValue = currencyFormatter.string(from: convertedValue)
        else {
            return ""
        }

        return formattedConvertedValue
    }

    var unformattedValue: String {
        guard
            let formattedValue = self.formattedValue,
            !formattedValue.isEmpty
        else {
            return ""
        }

        return formattedValue.components(separatedBy: CharacterSet(charactersIn: "\(currencySymbol)\(valueFormatter.groupingSeparator ?? "")")).joined()
    }

    var itemName: String?

    var title: String {
        switch itemMode {
        case .regular:
            return NSLocalizedString("New Regular", comment: "New Regular")
        default:
            return NSLocalizedString("New TransferWise Item", comment: "TransferWise Item")
        }
    }

    init() {
        dateFormatter.dateStyle = .medium
        let configuration = Configuration(bundle: Bundle(for: RestClient.self), name: "TWConfiguration")
        let client = RestClient(configuration: configuration)
        exchangeRatesService = ExchangeRatesService(client: client)
        valueFormatter = NumberFormatterFactory.formatter(type: .currency)
    }

    func save(context: NSManagedObjectContext? = nil) -> SaveItemError? {
        let storageManager = CoreDataManager.shared.storageManager
        let workingContext = context ?? storageManager.persistentContainer.viewContext
        let baseValue = itemMode == .transferWise ? convertedValue : value

        guard let name = self.itemName, !name.isEmpty else {
            return .noName
        }

        guard let value = baseValue else {
            return .noValue
        }

        guard
            let itemType = self.itemType,
            let itemMode = self.itemMode
        else {
            fatalError("Item type and mode are expected")
        }

        let day = Calendar.current.component(.day, from: date)
        let item = storageManager.insertItem(
            name: name,
            value: value.doubleValue,
            type: itemType,
            mode: itemMode,
            isActive: true,
            day: day,
            context: workingContext
        )

        if let item = item {
            let calculator = Calculator()
            calculator.updateBudget(with: item)
            storageManager.save()
        }

        return nil
    }

    func update(value text: String?) {
        guard let valueText = text,
            let value = numberFormatter.number(from: valueText)
        else {
            return
        }

        self.value = value
        convertValue()
    }

    func update(name: String?) {
        itemName = name
    }

    func update(date: Date) {
        self.date = date
    }

    func update(currency: Currency) {
        self.currency = currency
        valueFormatter.currencySymbol = currency.code
        convertValue()
    }

    func selectType(at index: Int) {
        guard index < ItemType.allTypes.count else {
            return
        }

        itemType = ItemType.allTypes[index]
    }

    private func convertValue() {
        guard
            let value = self.value,
            let source = self.currency?.code,
            let target = Locale.current.currencyCode
        else {
            convertedValue = NSNumber(value: 0)
            return
        }

        exchangeRatesService.rates(for: source, target: target) { [weak self] response, error in
            if let rate = response?.rate {
                self?.convertedValue = NSNumber(value: value.doubleValue * rate)
            }

            DispatchQueue.main.async {
                self?.delegate?.convertedValue(self?.convertedValue, error: error)
            }
        }
    }
}
