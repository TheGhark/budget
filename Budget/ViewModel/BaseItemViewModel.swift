//
//  BaseItemViewModel.swift
//  Budget
//
//  Created by Camilo Gaviria on 16/01/2020.
//  Copyright © 2020 heima. All rights reserved.
//

import CoreData
import Foundation

enum SaveItemError: Error {
    case noName
    case noValue

    var localizedDescription: String {
        switch self {
        case .noName:
            return NSLocalizedString("No name provided", comment: "The name of the item cannot be empty")
        case .noValue:
            return NSLocalizedString("No value provided", comment: "The value of the item cannot be empty")
        }
    }
}

protocol BaseItemViewModel {
    var itemType: ItemType? { get set }
    var itemMode: ItemMode? { get set }
    var date: Date { get }
    var formattedDate: String { get }
    var formattedValue: String? { get }
    var unformattedValue: String { get }
    var itemName: String? { get }

    func save(context: NSManagedObjectContext?) -> SaveItemError?
    func update(name: String?)
    func update(value: String?)
    func update(date: Date)
    func selectType(at: Int)
}
