//
//  ItemsViewModel.swift
//  Budget
//
//  Created by Camilo Gaviria on 10/01/2020.
//  Copyright © 2020 heima. All rights reserved.
//

import CoreData
import Foundation

enum SortBy: String {
    case value
    case date
    case active

    static let allValues = [value, date, active]

    var description: String {
        switch self {
        case .value:
            return NSLocalizedString("value", comment: "value")
        case .date:
            return NSLocalizedString("date", comment: "date")
        case .active:
            return NSLocalizedString("active", comment: "active")
        }
    }
}

enum ItemsError: Error {
    case cantPerfom
}

protocol ItemsViewModelDelegate: AnyObject {
    func viewModelWillChange()
    func viewModelDidChange()
    func viewModelDidChange(type: NSFetchedResultsChangeType, at indexPath: IndexPath?, newIndexPath: IndexPath?)
    func viewModelDidFailed(with error: ItemsError)
}

@objc class ItemsViewModel: NSObject {
    typealias ChangeHandler = () -> Void
    typealias DidChangeObjectHandler = (NSFetchedResultsChangeType, IndexPath?, IndexPath?) -> Void
    typealias ErrorHandler = (ItemsError) -> Void

    private var context: NSManagedObjectContext!
    private var storageManager = CoreDataManager.shared.storageManager
    private(set) var sortBy: SortBy = .value

    private let currencyFormatter = NumberFormatterFactory.formatter(type: .currency)
    private let ordinalFormatter = NumberFormatterFactory.formatter(type: .ordinal)
    private let calculator = Calculator()

    var itemType: ItemType = .credit
    weak var delegate: ItemsViewModelDelegate?

    private var predicate: NSPredicate {
        return NSPredicate(format: "type == %@", itemType.rawValue)
    }

    private var sortDescriptor: NSSortDescriptor {
        switch sortBy {
        case .value:
            return NSSortDescriptor(key: "value", ascending: false)
        case .date:
            return NSSortDescriptor(key: "day", ascending: true)
        case .active:
            return NSSortDescriptor(key: "active", ascending: false)
        }
    }

    fileprivate lazy var fetchedResultsController: NSFetchedResultsController<Item> = {
        let fetchRequest: NSFetchRequest<Item> = Item.fetchRequest()
        fetchRequest.sortDescriptors = [sortDescriptor]
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest,
                                                                  managedObjectContext: context,
                                                                  sectionNameKeyPath: nil,
                                                                  cacheName: nil)
        fetchedResultsController.delegate = self
        return fetchedResultsController
    }()

    var isEmpty: Bool {
        guard let fetchedObjects = fetchedResultsController.fetchedObjects else {
            return true
        }

        return fetchedObjects.isEmpty == true
    }

    var items: [Item] {
        return fetchedResultsController.fetchedObjects ?? []
    }

    var formattedActiveTotal: String? {
        var total: Double = 0

        for item in items where item.active {
            total += item.value
        }

        return currencyFormatter.string(from: NSNumber(value: total))
    }

    var formattedTotal: String? {
        var total: Double = 0

        for item in items {
            total += item.value
        }

        return currencyFormatter.string(from: NSNumber(value: total))
    }

    var title: String {
        switch itemType {
        case .credit:
            return NSLocalizedString("Outgoing", comment: "Outgoing")
        default:
            return NSLocalizedString("Incoming", comment: "Incoming")
        }
    }

    init(context: NSManagedObjectContext? = nil,
         delegate: ItemsViewModelDelegate? = nil) {
        self.delegate = delegate
        super.init()
        self.context = context ?? storageManager.persistentContainer.viewContext

        fetchedResultsController.delegate = self
    }

    func fetch() {
        do {
            fetchedResultsController.fetchRequest.predicate = predicate
            fetchedResultsController.fetchRequest.sortDescriptors = [sortDescriptor]
            try fetchedResultsController.performFetch()
        } catch {
            delegate?.viewModelDidFailed(with: .cantPerfom)
        }
    }

    func item(at indexPath: IndexPath) -> Item? {
        guard fetchedResultsController.fetchedObjects != nil else {
            return nil
        }

        return fetchedResultsController.object(at: indexPath)
    }

    func remove(at indexPath: IndexPath, context: NSManagedObjectContext? = nil) {
        guard let item = self.item(at: indexPath) else {
            return
        }

        item.active = false
        storageManager.remove(item: item, context: context)
        let calculator = Calculator()
        calculator.updateBudget(with: item)
        storageManager.save()
    }

    func value(of item: Item?) -> String? {
        guard let item = item else {
            return nil
        }

        return currencyFormatter.string(from: NSNumber(value: item.value))
    }

    func formattedDay(of item: Item?) -> String? {
        guard let day = item?.day else {
            return ""
        }

        return ordinalFormatter.string(from: NSNumber(value: day))
    }

    func toggle(at indexPath: IndexPath) {
        guard let item = self.item(at: indexPath) else {
            return
        }

        let active = item.active
        item.active = !active

        calculator.updateBudget(with: item)
        storageManager.save()
    }

    func sort(by index: Int) {
        sortBy = SortBy.allValues[index]
        fetch()
    }
}

extension ItemsViewModel: NSFetchedResultsControllerDelegate {
    func controllerWillChangeContent(_: NSFetchedResultsController<NSFetchRequestResult>) {
        delegate?.viewModelWillChange()
    }

    func controller(_: NSFetchedResultsController<NSFetchRequestResult>,
                    didChange _: Any,
                    at indexPath: IndexPath?,
                    for type: NSFetchedResultsChangeType,
                    newIndexPath: IndexPath?) {
        delegate?.viewModelDidChange(type: type, at: indexPath, newIndexPath: newIndexPath)
    }

    func controllerDidChangeContent(_: NSFetchedResultsController<NSFetchRequestResult>) {
        delegate?.viewModelDidChange()
    }
}
