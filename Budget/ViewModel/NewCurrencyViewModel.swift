//
//  NewCurrencyViewModel.swift
//  Budget
//
//  Created by Camilo Gaviria on 16/01/2020.
//  Copyright © 2020 heima. All rights reserved.
//

import CoreData
import Foundation

class NewCurrencyViewModel {
    private var name: String?
    private var code: String?

    func update(name: String?) {
        self.name = name
    }

    func update(code: String?) {
        self.code = code
    }

    func save(context: NSManagedObjectContext? = nil) {
        guard
            let name = self.name?.capitalized,
            let code = self.code?.uppercased()
        else {
            return
        }

        let storageManager = StorageManager()
        let workingContext = context ?? storageManager.persistentContainer.viewContext

        storageManager.insertCurrency(name: name, code: code, context: workingContext)
        storageManager.save(context: workingContext)
    }
}
