//
//  ItemViewModel.swift
//  Budget
//
//  Created by Camilo Gaviria on 10/01/2020.
//  Copyright © 2020 heima. All rights reserved.
//

import CoreData
import Foundation

enum ItemError: Error {
    case cantPerfom
}

@objc class ItemViewModel: NSObject, BaseItemViewModel {
    let numberFormatter = NumberFormatter()
    var itemType: ItemType? {
        get {
            guard let type = item.type else {
                return nil
            }
            return ItemType(rawValue: type)
        }
        set { item.type = newValue?.rawValue }
    }

    var itemMode: ItemMode? {
        get {
            guard let mode = item.mode else {
                return nil
            }
            return ItemMode(rawValue: mode)
        }
        set { item.mode = newValue?.rawValue }
    }

    var itemName: String?

    private var context: NSManagedObjectContext!
    private var storageManager = CoreDataManager.shared.storageManager
    private let currencyFormatter = NumberFormatterFactory.formatter(type: .currency)

    var item: Item!

    private let valueFormatter = NumberFormatterFactory.formatter(type: .currency)

    var day: Int16? {
        return item?.day
    }

    var date: Date {
        guard
            let day = self.day,
            let date = Calendar.current.date(bySetting: .day, value: Int(day), of: Date())
        else {
            return Date()
        }

        return date
    }

    private lazy var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale
        dateFormatter.dateStyle = .medium
        return dateFormatter
    }()

    var formattedDate: String {
        return dateFormatter.string(from: date)
    }

    var unformattedValue: String {
        guard let formattedValue = formattedValue,
            let currencySymbol = Locale.current.currencySymbol,
            !formattedValue.isEmpty
        else {
            return ""
        }

        return formattedValue.components(separatedBy: CharacterSet(charactersIn: "\(currencySymbol)\(currencyFormatter.groupingSeparator ?? "")")).joined()
    }

    var formattedValue: String? {
        return currencyFormatter.string(from: NSNumber(value: item.value))
    }

    init(context: NSManagedObjectContext? = nil) {
        super.init()
        self.context = context ?? storageManager.persistentContainer.viewContext
    }

    func save(context: NSManagedObjectContext?) -> SaveItemError? {
        guard item.name?.isEmpty == false else {
            return .noName
        }

        if let item = item {
            let calculator = Calculator()
            calculator.updateBudget(with: item)
            storageManager.save()
        }

        storageManager.save(context: context)
        return nil
    }

    func selectType(at index: Int) {
        guard index < ItemType.allTypes.count else {
            return
        }

        item.type = ItemType.allTypes[index].rawValue
    }

    func update(value text: String?) {
        guard let valueText = text,
            let value = numberFormatter.number(from: valueText)
        else {
            return
        }

        item.value = value.doubleValue
    }

    func update(name: String?) {
        item.name = name
    }

    func update(date: Date) {
        item.day = Int16(Calendar.current.component(.day, from: date))
    }
}
