//
//  BudgetViewModel.swift
//  Budget
//
//  Created by Camilo Gaviria on 10/01/2020.
//  Copyright © 2020 heima. All rights reserved.
//

import CoreData
import Foundation

class BudgetViewModel {
    let numberFormatter = NumberFormatter()
    typealias SaveHandler = () -> Void
    private let storageManager = CoreDataManager.shared.storageManager
    private let priceFormatter = NumberFormatterFactory.formatter(type: .currency)
    private let calculator = Calculator()
    private var budget: Budget?

    private var available: NSNumber {
        guard let budget = self.budget else {
            return NSNumber(value: 0)
        }
        return NSNumber(value: budget.available)
    }

    var groceries: Groceries? {
        return budget?.groceries
    }

    var totalCredits: String? {
        let total = storageManager.totalCredits()
        return priceFormatter.string(from: NSNumber(value: total))
    }

    var activeTotalDebits: String? {
        let total = storageManager.activeDebits()
        return priceFormatter.string(from: NSNumber(value: total))
    }

    var totalDebits: String? {
        let total = storageManager.totalDebits()
        return priceFormatter.string(from: NSNumber(value: total))
    }

    var currentCost: String? {
        let total = storageManager.activeCredits()
        return priceFormatter.string(from: NSNumber(value: total))
    }

    var formattedAvailableValue: String {
        guard let formattedAvailableValue = priceFormatter.string(from: available) else {
            return ""
        }

        return formattedAvailableValue
    }

    var unformattedAvailableValue: String {
        guard !formattedAvailableValue.isEmpty,
            let currencySymbol = Locale.current.currencySymbol else {
            return ""
        }

        return formattedAvailableValue
            .components(separatedBy: CharacterSet(charactersIn: "\(currencySymbol)\(priceFormatter.groupingSeparator ?? "")"))
            .joined()
    }

    var totalGroceries: String? {
        guard let groceries = budget?.groceries else {
            return nil
        }
        return priceFormatter.string(from: NSNumber(value: groceries.value * Double(groceries.count)))
    }

    var netTotal: String? {
        let totalDebits = storageManager.totalDebits()
        let totalCredits = storageManager.totalCredits()
        return priceFormatter.string(from: NSNumber(value: totalDebits - totalCredits))
    }

    var total: String? {
        guard let budget = self.budget else {
            return nil
        }
        return priceFormatter.string(from: NSNumber(value: budget.total))
    }

    var saveHandler: SaveHandler?

    init() {
        fetch()
    }

    func update(available text: String?) {
        guard let valueText = text,
            let available = numberFormatter.number(from: valueText)
        else {
            return
        }

        budget = calculator.updateBudget(available: available.doubleValue)
        saveHandler?()
    }

    func fetch() {
        budget = storageManager.fetchBudget()
    }
}
