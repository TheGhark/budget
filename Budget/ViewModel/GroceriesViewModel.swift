//
//  GroceriesViewModel.swift
//  Budget
//
//  Created by Camilo Gaviria on 10/01/2020.
//  Copyright © 2020 heima. All rights reserved.
//

import Foundation

protocol GroceriesViewModelDelegate: AnyObject {
    func viewModelDidChangeTotal(_: GroceriesViewModel)
}

class GroceriesViewModel {
    private let numberFormatter = NumberFormatter()
    private let currencyFormatter = NumberFormatterFactory.formatter(type: .currency)
    private let start: Date
    private let end: Date
    private var storageManager = CoreDataManager.shared.storageManager
    private var calculator: Calculator!

    var groceries: Groceries!

    weak var delegate: GroceriesViewModelDelegate?

    private var adjustment: Int {
        return Int(groceries.adjustment)
    }

    var amount: Double {
        return groceries.value
    }

    var formattedAmount: String? {
        return currencyFormatter.string(from: NSNumber(value: amount))
    }

    var unformattedAmount: String? {
        guard
            let formattedAmount = self.formattedAmount,
            !formattedAmount.isEmpty,
            let currencySymbol = Locale.current.currencySymbol,
            let groupSeparator = currencyFormatter.groupingSeparator
        else {
            return ""
        }

        let characterSet = CharacterSet(charactersIn: "\(currencySymbol)\(groupSeparator)")
        return formattedAmount.components(separatedBy: characterSet).joined()
    }

    var formattedAdjustment: String {
        if adjustment == 0 {
            return NSLocalizedString("Adjustment none", comment: "No change")
        } else if adjustment == 1 {
            return String(format: NSLocalizedString("Adjustment single", comment: "x day less"), "\(Int(adjustment))")
        } else {
            return String(format: NSLocalizedString("Adjustment plural", comment: "x days less"), "\(Int(adjustment))")
        }
    }

    var numberOfSaturdays: Int {
        if end.compare(start) == .orderedAscending {
            return 0
        }

        var count = 0
        var current = start

        while !sameDay(current, end) {
            if Calendar.current.component(.weekday, from: current) == 1 {
                count += 1
            }

            guard let newCurrent = Calendar.current.date(byAdding: .day, value: 1, to: current) else {
                fatalError("Impossible date")
            }

            current = newCurrent
        }

        return count
    }

    var total: Double {
        return Double(numberOfSaturdays - adjustment) * amount
    }

    var formattedTotal: String? {
        return currencyFormatter.string(from: NSNumber(value: total))
    }

    init(start: Date? = nil, end: Date? = nil, calculator: Calculator? = nil) {
        self.start = start ?? Date()

        if let end = end {
            self.end = end
        } else {
            let dayRange = Calendar.current.range(of: .day, in: .month, for: self.start)

            guard let dayCount = dayRange?.count else {
                fatalError("Impossible date")
            }

            var components = Calendar.current.dateComponents([.year, .month, .day], from: self.start)
            components.day = dayCount
            guard let end = Calendar.current.date(from: components) else {
                fatalError("Impossible date")
            }
            self.end = end
        }

        if groceries == nil {
            groceries = storageManager.insertGrocery()
        }

        let currentCount = numberOfSaturdays - adjustment
        var shouldSave = false

        if currentCount < 0 {
            groceries.count = Int16(numberOfSaturdays)
            shouldSave = true
        } else if groceries.count != currentCount {
            groceries.count = Int16(numberOfSaturdays - adjustment)
            shouldSave = true
        }

        if shouldSave {
            do {
                try groceries.managedObjectContext?.save()
            } catch {
                print("Cannot save groceries")
            }
        }

        self.calculator = calculator ?? Calculator()
    }

    func update(amount text: String?) {
        guard let valueText = text,
            let amount = numberFormatter.number(from: valueText)
        else {
            return
        }

        groceries.value = amount.doubleValue
        calculator.updateBudget(autoSave: false, groceries: groceries)
        storageManager.save()
    }

    func update(adjustment: Double) {
        groceries.adjustment = Int16(adjustment)
        calculator.updateBudget(autoSave: false, groceries: groceries)
        storageManager.save()

        delegate?.viewModelDidChangeTotal(self)
    }

    private func sameDay(_ current: Date, _ end: Date) -> Bool {
        let currentComponents = Calendar.current.dateComponents([.year, .month, .day], from: current)
        let endComponents = Calendar.current.dateComponents([.year, .month, .day], from: end)

        if currentComponents.year == endComponents.year,
            currentComponents.month == endComponents.month,
            currentComponents.day == endComponents.day {
            return true
        }

        return false
    }
}
