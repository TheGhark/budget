//
//  Date+Extensions.swift
//  Budget
//
//  Created by Camilo Gaviria on 10/01/2020.
//  Copyright © 2020 heima. All rights reserved.
//

import Foundation

extension Date {
    init?(_ dateString: String) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale

        guard let date = dateFormatter.date(from: dateString) else {
            return nil
        }

        self.init(timeInterval: 0, since: date)
    }
}
