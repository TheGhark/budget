//
//  Color.swift
//  Budget
//
//  Created by Camilo Gaviria on 22/01/2020.
//  Copyright © 2020 heima. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    static var budgetWhite: UIColor! {
        return UIColor(named: "BudgetWhite")
    }

    static var budgetDarkGreen: UIColor! {
        return UIColor(named: "BudgetDarkGreen")
    }

    static var budgetLightGreen: UIColor! {
        return UIColor(named: "BudgetLightGreen")
    }

    static var budgetGrey: UIColor! {
        return UIColor(named: "BudgetGrey")
    }

    static var budgetLightGrey: UIColor! {
        return UIColor(named: "BudgetLightGrey")
    }

    static var budgetRed: UIColor! {
        return UIColor(named: "BudgetRed")
    }

    static var budgetBlack: UIColor! {
        return UIColor(named: "BudgetBlack")
    }

    static var budgetBlue: UIColor! {
        return UIColor(named: "BudgetBlue")
    }
}
