//
//  NumberFormatter+Extensions.swift
//  Budget
//
//  Created by Camilo Gaviria on 10/01/2020.
//  Copyright © 2020 heima. All rights reserved.
//

import Foundation

enum NumberFormatterType {
    case currency
    case ordinal
}

class NumberFormatterFactory {
    static func formatter(type: NumberFormatterType) -> NumberFormatter {
        let formatter = NumberFormatter()

        switch type {
        case .currency:
            formatter.currencySymbol = Locale.current.currencySymbol
            formatter.numberStyle = .currency
        case .ordinal:
            formatter.numberStyle = .ordinal
        }

        return formatter
    }
}
