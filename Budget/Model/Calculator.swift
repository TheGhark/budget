//
//  Calculator.swift
//  Budget
//
//  Created by Camilo Gaviria on 16/01/2020.
//  Copyright © 2020 heima. All rights reserved.
//

import CoreData
import Foundation

class Calculator {
    // MARK: - Class Functions

    // MARK: - Properties

    private let storageManager = CoreDataManager.shared.storageManager

    // MARK: - Computed Properties

    // MARK: - Initialization

    // MARK: - Public

    @discardableResult
    func updateBudget(autoSave: Bool = true, available: Double? = nil, groceries: Groceries? = nil) -> Budget? {
        guard let budget = storageManager.fetchBudget(context: groceries?.managedObjectContext) else {
            return nil
        }

        let costs = storageManager.activeCredits(context: budget.managedObjectContext)

        let currentAvailable: Double
        let currentGrocery: Groceries

        if let available = available {
            budget.available = available
            currentAvailable = available
        } else {
            currentAvailable = budget.available
        }

        if let groceries = groceries {
            budget.groceries = groceries
            currentGrocery = groceries
        } else if let groceries = budget.groceries {
            currentGrocery = groceries
        } else {
            guard let groceries = storageManager.insertGrocery(context: budget.managedObjectContext) else {
                fatalError("Cannot insert groceries")
            }

            currentGrocery = groceries
        }

        budget.total = currentAvailable - (currentGrocery.value * Double(currentGrocery.count)) - costs

        if autoSave {
            do {
                try budget.managedObjectContext?.save()
            } catch {
                print("Cannot save budget")
            }
        }

        return budget
    }

    @discardableResult
    func updateBudget(autoSave: Bool = true, with item: Item, context: NSManagedObjectContext? = nil) -> Budget? {
        let workingContext = context ?? storageManager.persistentContainer.viewContext

        guard let budget = storageManager.fetchBudget(context: workingContext),
            let type = item.type,
            let itemType = ItemType(rawValue: type),
            let groceries = budget.groceries
        else {
            return nil
        }

        let costs = storageManager.activeCredits(context: workingContext)
        budget.total = budget.available - (groceries.value * Double(groceries.count)) - costs

        switch itemType {
        case .debit:
            if item.active {
                budget.total += item.value
            }
        default:
            if item.active {
                budget.total -= item.value
            } else {
                budget.total += item.value
            }
        }

        if autoSave {
            do {
                try item.managedObjectContext?.save()
                try budget.managedObjectContext?.save()
            } catch {
                print("Cannot save: \(error)")
            }
        }

        return budget
    }

    // MARK: - Private

    // MARK: - Overridden
}
