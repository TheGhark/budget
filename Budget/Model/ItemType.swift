//
//  ItemType.swift
//  Budget
//
//  Created by Camilo Gaviria on 20/01/2020.
//  Copyright © 2020 heima. All rights reserved.
//

import Foundation

enum ItemType: String {
    case credit
    case debit

    static let allTypes = [credit, debit]

    var description: String {
        switch self {
        case .credit:
            return NSLocalizedString("credit", comment: "credit")
        case .debit:
            return NSLocalizedString("debit", comment: "debit")
        }
    }
}
