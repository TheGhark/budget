//
//  BudgetPersistentContainer.swift
//  Budget
//
//  Created by Camilo Gaviria on 20/01/2020.
//  Copyright © 2020 heima. All rights reserved.
//

import CoreData
import UIKit

class BudgetPersistentContainer: NSPersistentContainer {
    override class func defaultDirectoryURL() -> URL {
        guard let url = FileManager.default.containerURL(forSecurityApplicationGroupIdentifier: "group.heima.budget") else {
            fatalError("No app group found")
        }

        return url
    }
}
