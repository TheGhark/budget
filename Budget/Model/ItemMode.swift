//
//  ItemMode.swift
//  Budget
//
//  Created by Camilo Gaviria on 20/01/2020.
//  Copyright © 2020 heima. All rights reserved.
//

import Foundation

enum ItemMode: String {
    case regular
    case transferWise
}
