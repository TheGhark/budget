//
//  StoreManager.swift
//  Budget
//
//  Created by Camilo Gaviria on 10/01/2020.
//  Copyright © 2020 heima. All rights reserved.
//

import BorderlessAccount
import CoreData
import Foundation
import UIKit

class StorageManager {
    // MARK: - Class Functions

    // MARK: - Properties

    let persistentContainer: NSPersistentContainer

    // MARK: - Computed Properties

    // MARK: - Initialization

    init(persistentContainer: NSPersistentContainer = CoreDataManager.shared.persistentContainer) {
        self.persistentContainer = persistentContainer
        self.persistentContainer.viewContext.automaticallyMergesChangesFromParent = true
    }

    // MARK: - Public

    func activeDebits(context: NSManagedObjectContext? = nil) -> Double {
        return total(with: NSPredicate(format: "type == %@ && active == true", ItemType.debit.rawValue), context: context)
    }

    func activeCredits(context: NSManagedObjectContext? = nil) -> Double {
        return total(with: NSPredicate(format: "type == %@ && active == true", ItemType.credit.rawValue), context: context)
    }

    func totalCredits(context: NSManagedObjectContext? = nil) -> Double {
        return total(with: NSPredicate(format: "type == %@", ItemType.credit.rawValue), context: context)
    }

    func totalDebits(context: NSManagedObjectContext? = nil) -> Double {
        return total(with: NSPredicate(format: "type == %@", ItemType.debit.rawValue), context: context)
    }

    func save(context: NSManagedObjectContext? = nil) {
        let workingContext = context ?? persistentContainer.viewContext

        if workingContext.hasChanges {
            do {
                try workingContext.save()
            } catch {
                print("Could not save: \(error)")
            }
        }
    }

    @discardableResult
    func insertBudget(context: NSManagedObjectContext? = nil) -> Budget? {
        let workingContext = context ?? persistentContainer.viewContext

        if let existingBudget = fetchBudget(context: workingContext) {
            if existingBudget.groceries == nil {
                insertGrocery(context: workingContext)
            } else if existingBudget.groceries?.count == 0 {
                existingBudget.groceries?.count = 1
            }

            return existingBudget
        }

        guard let newBudget: Budget = insert(context: workingContext) else {
            return nil
        }

        if let groceries = insertGrocery() {
            groceries.setValue(newBudget, forKey: "budget")
            newBudget.setValue(groceries, forKey: "groceries")
        }

        return newBudget
    }

    func insert(currencies: [BorderlessAccount.Currency], context: NSManagedObjectContext? = nil) {
        let workingContext = context ?? persistentContainer.viewContext

        for currency in currencies {
            if let fetchCurrency = fetchCurrency(with: currency.code, context: workingContext) {
                fetchCurrency.name = currency.name
                continue
            }

            guard let newCurrency: Currency = insert(context: workingContext) else {
                continue
            }

            newCurrency.setValue(currency.code, forKey: "code")
            newCurrency.setValue(currency.name, forKey: "name")
        }
    }

    @discardableResult
    func insertCurrency(name: String, code: String, context: NSManagedObjectContext? = nil) -> Currency? {
        let workingContext = context ?? persistentContainer.viewContext

        guard let newCurrency: Currency = insert(context: workingContext) else {
            return nil
        }

        newCurrency.setValue(code, forKey: "code")
        newCurrency.setValue(name, forKey: "name")

        return newCurrency
    }

    @discardableResult
    func insertGrocery(amount: Double = 0,
                       adjustment: Int = 0,
                       count: Int = 1,
                       context: NSManagedObjectContext? = nil) -> Groceries? {
        let workingContext = context ?? persistentContainer.viewContext

        guard let newGrocery: Groceries = insert(context: workingContext) else {
            return nil
        }

        newGrocery.setValue(amount, forKey: "value")
        newGrocery.setValue(adjustment, forKey: "adjustment")
        newGrocery.setValue(count, forKey: "count")

        if let budget = fetchBudget(context: workingContext) {
            newGrocery.setValue(budget, forKey: "budget")
            budget.setValue(newGrocery, forKey: "groceries")
        }

        return newGrocery
    }

    @discardableResult
    func insertItem(name: String,
                    value: Double,
                    type: ItemType,
                    mode: ItemMode = .regular,
                    isActive: Bool,
                    day: Int,
                    context: NSManagedObjectContext? = nil) -> Item? {
        let workingContext = context ?? persistentContainer.viewContext

        guard let newItem: Item = insert(context: workingContext) else {
            return nil
        }

        newItem.setValue(name, forKey: "name")
        newItem.setValue(value, forKey: "value")
        newItem.setValue(type.rawValue, forKey: "type")
        newItem.setValue(mode.rawValue, forKey: "mode")
        newItem.setValue(isActive, forKey: "active")
        newItem.setValue(day, forKey: "day")

        return newItem
    }

    @discardableResult
    func fetchCredits(context: NSManagedObjectContext? = nil) -> [Item] {
        let workingContext = context ?? persistentContainer.viewContext
        return fetchItems(with: NSPredicate(format: "type == %@", ItemType.credit.rawValue), context: workingContext)
    }

    func fetchBudget(context: NSManagedObjectContext? = nil) -> Budget? {
        let workingContext = context ?? persistentContainer.viewContext

        let fetchRequest: NSFetchRequest<Budget> = NSFetchRequest(entityName: "Budget")

        do {
            let result = try workingContext.fetch(fetchRequest)
            return result.first
        } catch {
            print("Could not fetch credit: \(error)")
        }

        return nil
    }

    func fetchCurrencies(context: NSManagedObjectContext? = nil) -> [Currency] {
        let workingContext = context ?? persistentContainer.viewContext

        let fetchRequest: NSFetchRequest<Currency> = NSFetchRequest(entityName: "Currency")
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "code", ascending: true)]

        do {
            return try workingContext.fetch(fetchRequest)
        } catch {
            print("Could not fetch credit: \(error)")
        }

        return []
    }

    func remove(item: Item, context: NSManagedObjectContext? = nil) {
        remove(objectID: item.objectID, context: context)
    }

    // MARK: - Private

    private func total(with predicate: NSPredicate, context: NSManagedObjectContext? = nil) -> Double {
        let workingContext = context ?? persistentContainer.viewContext
        let request: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: "Item")
        request.resultType = NSFetchRequestResultType.dictionaryResultType
        request.predicate = predicate

        let expressionDescription = NSExpressionDescription()
        expressionDescription.name = "total"
        expressionDescription.expression = NSExpression(forKeyPath: "value")
        expressionDescription.expressionResultType = .decimalAttributeType

        request.propertiesToFetch = [expressionDescription]

        do {
            guard let results = try workingContext.fetch(request) as? [[String: Double]] else {
                return 0
            }

            let totals: [Double] = results.compactMap { $0["total"] }
            var sum: Double = 0

            for total in totals {
                sum += total
            }

            return sum
        } catch {
            print("Could not fetch credit: \(error)")
        }

        return 0
    }

    private func insert<E: NSManagedObject>(context: NSManagedObjectContext) -> E? {
        guard let entity = NSEntityDescription.insertNewObject(forEntityName: String(describing: E.self), into: context) as? E else {
            return nil
        }

        return entity
    }

    private func fetchItems(with predicate: NSPredicate? = nil, context: NSManagedObjectContext? = nil) -> [Item] {
        let workingContext = context ?? persistentContainer.viewContext

        let fetchRequest: NSFetchRequest<Item> = NSFetchRequest(entityName: "Item")

        if let predicate = predicate {
            fetchRequest.predicate = predicate
        }

        do {
            let result = try workingContext.fetch(fetchRequest)
            return result
        } catch {
            print("Could not fetch debit: \(error)")
        }

        return []
    }

    private func fetchCurrency(with code: String, context: NSManagedObjectContext? = nil) -> Currency? {
        let workingContext = context ?? persistentContainer.viewContext

        let fetchRequest: NSFetchRequest<Currency> = NSFetchRequest(entityName: "Currency")
        fetchRequest.predicate = NSPredicate(format: "code == %@", code)

        do {
            let result = try workingContext.fetch(fetchRequest)
            return result.first
        } catch {
            print("Could not fetch debit: \(error)")
        }

        return nil
    }

    private func remove(objectID: NSManagedObjectID, context: NSManagedObjectContext? = nil) {
        let workingContext = context ?? persistentContainer.viewContext
        let object = workingContext.object(with: objectID)
        workingContext.delete(object)
    }

    // MARK: - Overridden
}
