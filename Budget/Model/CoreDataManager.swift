//
//  CoreDataManager.swift
//  Budget
//
//  Created by Camilo Gaviria on 10/01/2020.
//  Copyright © 2020 heima. All rights reserved.
//

import Base
import BorderlessAccount
import CoreData
import Foundation

class CoreDataManager {
    // MARK: - Class Functions

    static var shared: CoreDataManager {
        let configuration = Configuration(bundle: Bundle(for: RestClient.self), name: "TWConfiguration")
        let client = RestClient(configuration: configuration)
        let borderlessClient = BorderlessAccountService(client: client)
        return CoreDataManager(borderlessAccountService: borderlessClient)
    }

    private static func defaultContainer(name: String) -> BudgetPersistentContainer {
        let container = BudgetPersistentContainer(name: name)
        container.loadPersistentStores(completionHandler: { _, error in
            if let error = error as NSError? {
                fatalError("Could not load the persistent store with name \(name). Error: \(error)")
            }
        })
        return container
    }

    // MARK: - Properties

    let persistentContainer: NSPersistentContainer
    let storageManager: StorageManager
    private let borderlessAccountService: BorderlessAccountService

    // MARK: - Computed Properties

    // MARK: - Initialization

    init(persistentContainer: BudgetPersistentContainer? = nil,
         storageManager: StorageManager? = nil,
         containerName: String = "Budget",
         borderlessAccountService: BorderlessAccountService) {
        let workingPersistentContainer = persistentContainer ?? CoreDataManager.defaultContainer(name: containerName)
        self.storageManager = storageManager ?? StorageManager(persistentContainer: workingPersistentContainer)
        self.borderlessAccountService = borderlessAccountService
        self.persistentContainer = workingPersistentContainer
    }

    // MARK: - Public

    func initialize(completion: @escaping (Error?) -> Void) {
        storageManager.insertBudget(context: persistentContainer.viewContext)
        updateCurrencies(context: persistentContainer.viewContext, completion: completion)
        storageManager.save(context: persistentContainer.viewContext)
    }

    func save() {
        storageManager.save()
    }

    // MARK: - Private

    private func updateCurrencies(context: NSManagedObjectContext, completion: @escaping (Error?) -> Void) {
        borderlessAccountService.currencies { [unowned self] response, error in
            if let error = error {
                completion(error)
            } else if let response = response {
                self.storageManager.insert(currencies: response.currencies, context: context)
                self.storageManager.save(context: context)
                completion(nil)
            }
        }
    }

    // MARK: - Overridden
}
