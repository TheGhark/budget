//
//  SceneDelegate.swift
//  Budget
//
//  Created by Camilo Gaviria on 17/12/2019.
//  Copyright © 2019 heima. All rights reserved.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    var window: UIWindow?

    func sceneDidEnterBackground(_: UIScene) {
        (UIApplication.shared.delegate as? AppDelegate)?.coreDataManager.save()
    }
}
