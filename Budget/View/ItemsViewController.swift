//
//  ItemsViewController.swift
//  Budget
//
//  Created by Camilo Gaviria on 10/01/2020.
//  Copyright © 2020 heima. All rights reserved.
//

import CoreData
import UIKit

class ItemsViewController: UIViewController {
    private let viewModel = ItemsViewModel()
    private let cellIdentifier = "ItemsCellIdentifier"
    private let headerIdentifier = "ItemsHeaderViewIdentifier"
    private let itemSegueIdentifier = "ItemSegueIdentifier"
    private let newItemSegueIdentifier = "NewItemSegueIdentifier"
    private let newTransferWiseItemSegueIdentifier = "TransferWiseNewItemSegueIdentifier"

    private var headerView: ItemsHeaderView?

    @IBOutlet private var tableView: UITableView!

    var itemType: ItemType {
        get { return viewModel.itemType }
        set { viewModel.itemType = newValue }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        title = viewModel.title
        viewModel.delegate = self

        setupTableView()
        setupRightBarButtonItems()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.fetch()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == itemSegueIdentifier {
            guard
                let indexPath = sender as? IndexPath,
                let item = viewModel.item(at: indexPath) else {
                return
            }

            let itemViewController = (segue.destination as? UINavigationController)?.topViewController as? ItemTableViewController
            itemViewController?.item = item
        } else if segue.identifier == newItemSegueIdentifier {
            let newItemViewController = (segue.destination as? UINavigationController)?.topViewController as? NewItemTableViewController
            newItemViewController?.itemType = viewModel.itemType
            newItemViewController?.itemMode = .regular
        } else if segue.identifier == newTransferWiseItemSegueIdentifier {
            let newItemViewController = (segue.destination as? UINavigationController)?.topViewController as? NewItemTableViewController
            newItemViewController?.itemType = viewModel.itemType
            newItemViewController?.itemMode = .transferWise
        }

        super.prepare(for: segue, sender: sender)
    }

    @objc private func handleRefreshController() {
        DispatchQueue.main.async {
            self.viewModel.fetch()
            self.tableView.refreshControl?.endRefreshing()
            self.tableView.reloadData()
        }
    }

    @objc func editButtonTapped(_ sender: UIBarButtonItem) {
        tableView.isEditing = !tableView.isEditing
        sender.title = tableView.isEditing ? NSLocalizedString("Done", comment: "Done") : NSLocalizedString("Edit", comment: "Edit")
    }

    @IBAction func sortByValueChanged(_ sender: UISegmentedControl) {
        viewModel.sort(by: sender.selectedSegmentIndex)
        tableView.reloadData()
    }

    @objc private func newItemButtonTapped(_ sender: UISegmentedControl) {
        let alertController = UIAlertController(
            title: NSLocalizedString("New item", comment: "New item"),
            message: NSLocalizedString("New item message", comment: "Select the type of new item to add"),
            preferredStyle: .actionSheet
        )
        alertController.addAction(UIAlertAction(
            title: NSLocalizedString("Regular", comment: "Regular"),
            style: .default,
            handler: { [unowned self] _ in
                self.performSegue(withIdentifier: self.newItemSegueIdentifier, sender: sender)
            }
        ))
        alertController.addAction(UIAlertAction(
            title: NSLocalizedString("TransferWise", comment: "TransferWise"),
            style: .default,
            handler: { [unowned self] _ in
                self.performSegue(withIdentifier: self.newTransferWiseItemSegueIdentifier, sender: sender)
            }
        ))
        alertController.addAction(UIAlertAction(
            title: NSLocalizedString("Cancel", comment: "Cancel"),
            style: .cancel,
            handler: { _ in
                alertController.dismiss(animated: true, completion: nil)
            }
        ))
        present(alertController, animated: true, completion: nil)
    }

    private func setupTableView() {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(handleRefreshController), for: .valueChanged)
        tableView.refreshControl = refreshControl

        let nib = UINib(nibName: String(describing: ItemsHeaderView.self), bundle: nil)
        tableView.register(nib, forHeaderFooterViewReuseIdentifier: headerIdentifier)

        tableView.tableFooterView = UIView()
    }

    private func setupRightBarButtonItems() {
        let addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(newItemButtonTapped(_:)))

        navigationItem.rightBarButtonItems = [editButtonItem, addButton]
        editButtonItem.action = #selector(editButtonTapped(_:))
    }
}

extension ItemsViewController: UITableViewDataSource {
    func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
        return viewModel.items.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? ItemTableViewCell else {
            fatalError("Must have cells")
        }

        let item = viewModel.item(at: indexPath)
        cell.update(with: item, value: viewModel.value(of: item), formattedDay: viewModel.formattedDay(of: item))
        return cell
    }

    func tableView(_: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            viewModel.remove(at: indexPath)
        }
    }
}

extension ItemsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView.isEditing {
            performSegue(withIdentifier: itemSegueIdentifier, sender: indexPath)
        } else {
            viewModel.toggle(at: indexPath)
            tableView.reloadRows(at: [indexPath], with: .automatic)
            headerView?.update(with: viewModel.formattedTotal, activeTotal: viewModel.formattedActiveTotal, sortBy: viewModel.sortBy)
        }

        tableView.deselectRow(at: indexPath, animated: true)
    }

    func tableView(_: UITableView, heightForHeaderInSection _: Int) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_: UITableView, estimatedHeightForHeaderInSection _: Int) -> CGFloat {
        return 85
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection _: Int) -> UIView? {
        headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: headerIdentifier) as? ItemsHeaderView
        headerView?.update(with: viewModel.formattedTotal, activeTotal: viewModel.formattedActiveTotal, sortBy: viewModel.sortBy)
        headerView?.delegate = self
        return headerView
    }
}

extension ItemsViewController: ItemsViewModelDelegate {
    func viewModelWillChange() {
        tableView.beginUpdates()
    }

    func viewModelDidChange() {
        tableView.endUpdates()
    }

    func viewModelDidChange(type: NSFetchedResultsChangeType, at indexPath: IndexPath?, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            if let indexPath = newIndexPath {
                tableView.insertRows(at: [indexPath], with: .automatic)
            }
        case .delete:
            if let indexPath = indexPath {
                tableView.deleteRows(at: [indexPath], with: .automatic)
            }
        case .update:
            if let indexPath = indexPath {
                tableView.reloadRows(at: [indexPath], with: .automatic)
            }
        case .move:
            if let indexPath = indexPath, let newIndexPath = newIndexPath {
                tableView.reloadRows(at: [indexPath, newIndexPath], with: .automatic)
            }
        default:
            print("Unsupported change type: \(type)")
        }
    }

    func viewModelDidFailed(with error: ItemsError) {
        let alertController = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
        present(alertController, animated: true, completion: nil)
    }
}

extension ItemsViewController: ItemsHeaderViewDelegate {
    func itemsHeaderView(_: ItemsHeaderView, didChangeSortBy index: Int) {
        viewModel.sort(by: index)
        tableView.reloadData()
    }
}
