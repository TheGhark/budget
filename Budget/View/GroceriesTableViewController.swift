//
//  GroceriesTableViewController.swift
//  Budget
//
//  Created by Camilo Gaviria on 10/01/2020.
//  Copyright © 2020 heima. All rights reserved.
//

import UIKit

class GroceriesTableViewController: UITableViewController {
    @IBOutlet private var budgetTextField: UITextField!
    @IBOutlet private var numberLabel: UILabel!
    @IBOutlet private var totalLabel: UILabel!
    @IBOutlet private var stepper: UIStepper!
    @IBOutlet private var adjustmentLabel: UILabel!

    let numberFormatter = NumberFormatter()

    var groceries: Groceries {
        get { return viewModel.groceries }
        set { viewModel.groceries = newValue }
    }

    private let viewModel = GroceriesViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()

        viewModel.delegate = self
        budgetTextField.delegate = self
        adjustmentLabel.text = viewModel.formattedAdjustment
        update()
    }

    @IBAction func budgetTextFieldDidEndEditing(_ sender: UITextField) {
        viewModel.update(amount: sender.text)
        update()
        editButtonItem.title = NSLocalizedString("Edit", comment: "Edit")
    }

    @objc private func dismissKeyboard() {
        budgetTextField.resignFirstResponder()
    }

    @IBAction func stepperValueDidChanged(_ sender: UIStepper) {
        viewModel.update(adjustment: sender.value)
        adjustmentLabel.text = viewModel.formattedAdjustment
    }

    private func update() {
        budgetTextField.text = viewModel.formattedAmount
        numberLabel.text = "\(viewModel.numberOfSaturdays)"
        totalLabel.text = viewModel.formattedTotal
        stepper.maximumValue = Double(viewModel.numberOfSaturdays)
    }
}

extension GroceriesTableViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(dismissKeyboard))
        toolbar.items = [flexSpace, doneButton]
        toolbar.sizeToFit()
        textField.inputAccessoryView = toolbar

        textField.text = viewModel.unformattedAmount
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn _: NSRange, replacementString string: String) -> Bool {
        guard !string.isEmpty else {
            return true
        }

        guard let text = textField.text, !text.isEmpty else {
            return true
        }

        let number = numberFormatter.number(from: text + string)
        return number != nil
    }

    func textFieldShouldReturn(_: UITextField) -> Bool {
        return true
    }
}

extension GroceriesTableViewController: GroceriesViewModelDelegate {
    func viewModelDidChangeTotal(_: GroceriesViewModel) {
        update()
    }
}
