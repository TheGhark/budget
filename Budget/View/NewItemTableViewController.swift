//
//  NewTransactionTableViewController.swift
//  Expenses
//
//  Created by Camilo Gaviria on 06/12/2019.
//  Copyright © 2019 Heima. All rights reserved.
//

import UIKit

class NewItemTableViewController: BaseItemTableViewController {
    private let currenciesSegueIdentifier = "CurrenciesSegueIdentifier"
    private let newItemViewModel = NewItemViewModel()

    override var viewModel: BaseItemViewModel! {
        get { return newItemViewModel }
        set {}
    }

    @IBOutlet var convertedValueLabel: UILabel?

    private var currencyLabel: UILabel!
    private var currencyImage: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()

        newItemViewModel.delegate = self
    }

    override func setupAppeareance() {
        super.setupAppeareance()

        valueTextField.rightViewMode = itemMode == .transferWise ? .always : .never
        setupCurrenciesView()

        title = newItemViewModel.title
        convertedValueLabel?.textColor = .budgetGrey
        convertedValueLabel?.text = newItemViewModel.formattedConvertedValue
    }

    private func setupCurrenciesView() {
        let rightView = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 30))
        rightView.backgroundColor = .budgetLightGreen
        rightView.layer.cornerRadius = 5
        rightView.layer.masksToBounds = true

        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(showCurrencies))
        rightView.addGestureRecognizer(tapRecognizer)

        let currencyImage = UIImageView(image: UIImage(named: "currency"))
        currencyImage.contentMode = .center
        currencyImage.frame = CGRect(x: 0,
                                     y: 0,
                                     width: 40,
                                     height: 30)
        self.currencyImage = currencyImage

        let currencyLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 40, height: 30))
        currencyLabel.font = UIFont.systemFont(ofSize: 13, weight: .semibold)
        currencyLabel.textColor = .budgetWhite
        currencyLabel.isHidden = true
        currencyLabel.textAlignment = .center
        self.currencyLabel = currencyLabel

        rightView.addSubview(currencyImage)
        rightView.addSubview(currencyLabel)

        valueTextField.rightView = rightView
    }

    override func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
        if isDatePickerVisible {
            return itemMode == .transferWise ? 6 : 4
        }

        return itemMode == .transferWise ? 5 : 3
    }

    @IBAction func unwindToNewTransferWiseItem(segue: UIStoryboardSegue) {
        guard let currency = (segue.source as? CurrenciesTableViewController)?.selectedCurrency else {
            return
        }

        newItemViewModel.update(currency: currency)
        currencyImage.isHidden = true
        currencyLabel.isHidden = false
        currencyLabel.text = currency.code
    }

    @objc func showCurrencies() {
        performSegue(withIdentifier: currenciesSegueIdentifier, sender: self)
    }
}

extension NewItemTableViewController: NewItemViewModelDelegate {
    func convertedValue(_: NSNumber?, error: Error?) {
        if let error = error {
            let alertControler = UIAlertController(title: "Could not convert currency", message: error.localizedDescription, preferredStyle: .alert)
            alertControler.addAction(UIAlertAction(title: "Ok", style: .default, handler: { _ in
                alertControler.dismiss(animated: true, completion: nil)
            }))
            present(alertControler, animated: true, completion: nil)
        }

        convertedValueLabel?.text = newItemViewModel.formattedConvertedValue
        valueTextField.text = viewModel.formattedValue
    }
}
