//
//  NewCurrencyTableViewController.swift
//  Budget
//
//  Created by Camilo Gaviria on 16/01/2020.
//  Copyright © 2020 heima. All rights reserved.
//

import UIKit

class NewCurrencyTableViewController: UITableViewController {
    @IBOutlet private var nameTextField: UITextField!
    @IBOutlet private var codeTextField: UITextField!

    private let unwindSegueIdentifier = "UnwindToCurrenciesSegueIdentifier"

    private let viewModel = NewCurrencyViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()

        title = NSLocalizedString("New Currency", comment: "New Currency")
        tableView.tableFooterView = UIView()
    }

    @IBAction func nameEditingDidEnd(_ sender: UITextField) {
        viewModel.update(name: sender.text)
    }

    @IBAction func codeEditingDidEnd(_ sender: UITextField) {
        viewModel.update(code: sender.text)
    }

    @IBAction func saveButtonTapped(_ sender: UIBarButtonItem) {
        if nameTextField.canResignFirstResponder {
            nameTextField.resignFirstResponder()
        }

        if codeTextField.canResignFirstResponder {
            codeTextField.resignFirstResponder()
        }

        viewModel.save()
        performSegue(withIdentifier: unwindSegueIdentifier, sender: sender)
    }
}

extension NewCurrencyTableViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn _: NSRange, replacementString string: String) -> Bool {
        if textField == codeTextField, !string.isEmpty {
            let newString = "\(textField.text ?? "")\(string)"
            return newString.count <= 3
        } else {
            return true
        }
    }
}
