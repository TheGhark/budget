//
//  BaseItemTableViewController.swift
//  Budget
//
//  Created by Camilo Gaviria on 16/01/2020.
//  Copyright © 2020 heima. All rights reserved.
//

import UIKit

class BaseItemTableViewController: UITableViewController {
    let numberFormatter = NumberFormatter()

    let dateCellIdentifier = "DateCellIdentifier"
    private let datePickerCellIdentifier = "DatePickerCellIdentifier"
    var isDatePickerVisible = false

    @IBOutlet var nameTextField: UITextField!
    @IBOutlet var valueTextField: UITextField!
    @IBOutlet var typeSegementedControl: UISegmentedControl?
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var datePicker: UIDatePicker!

    var viewModel: BaseItemViewModel!

    var itemType: ItemType? {
        get { return viewModel.itemType }
        set { viewModel.itemType = newValue }
    }

    var itemMode: ItemMode? {
        get { return viewModel.itemMode }
        set { viewModel.itemMode = newValue }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        setupAppeareance()
    }

    func setupAppeareance() {
        tableView.tableFooterView = UIView()
        datePicker.date = viewModel.date
        dateLabel.text = viewModel.formattedDate
        dateLabel.textColor = .budgetBlue

        nameTextField.delegate = self
        valueTextField.delegate = self

        if let typeSegementedControl = self.typeSegementedControl {
            typeSegementedControl.removeAllSegments()
            for (index, type) in ItemType.allTypes.enumerated() {
                typeSegementedControl.insertSegment(withTitle: type.description, at: index, animated: false)

                if type == viewModel.itemType {
                    typeSegementedControl.selectedSegmentIndex = index
                }
            }
        }
    }

    @IBAction func cancelButtonTapped(_: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }

    @discardableResult
    override func resignFirstResponder() -> Bool {
        if nameTextField.isFirstResponder {
            nameTextField.resignFirstResponder()
        } else if valueTextField.isFirstResponder {
            valueTextField.resignFirstResponder()
        }

        return super.resignFirstResponder()
    }

    @IBAction func saveButtonTapped(_: UIBarButtonItem) {
        resignFirstResponder()

        let result = viewModel.save(context: nil)

        if let result = result {
            let alert = UIAlertController(title: NSLocalizedString("Cannot save", comment: "Cannot create a new item"),
                                          message: result.localizedDescription,
                                          preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("Ok", comment: "Ok"),
                                          style: .default, handler: nil))
            present(alert, animated: true, completion: nil)
        } else {
            dismiss(animated: true, completion: nil)
        }
    }

    @IBAction func valueEditingDidEnd(_ sender: UITextField) {
        viewModel.update(value: sender.text)
        sender.text = viewModel.formattedValue
    }

    @IBAction func nameEditingDidEnd(_ sender: UITextField) {
        viewModel.update(name: sender.text)

        if viewModel.itemName?.isEmpty == false {
            title = viewModel.itemName
        }
    }

    @IBAction func typeSegmentedControlChanged(_ sender: UISegmentedControl) {
        viewModel.selectType(at: sender.selectedSegmentIndex)
    }

    @IBAction func datePickerValueChanged(_ sender: UIDatePicker) {
        viewModel.update(date: sender.date)
        dateLabel.text = viewModel.formattedDate
    }

    @objc private func dismissKeyboard() {
        resignFirstResponder()
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        resignFirstResponder()

        let cell = tableView.cellForRow(at: indexPath)

        if cell?.reuseIdentifier == dateCellIdentifier {
            isDatePickerVisible = !isDatePickerVisible
            tableView.beginUpdates()

            let index = tableView.numberOfRows(inSection: indexPath.section)

            if isDatePickerVisible {
                dateLabel.textColor = .budgetRed
                tableView.insertRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
            } else {
                dateLabel.textColor = .budgetBlue
                tableView.deleteRows(at: [IndexPath(row: index - 1, section: 0)], with: .automatic)
            }

            tableView.endUpdates()
        }

        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension BaseItemTableViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == valueTextField {
            let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
            let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
            let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(dismissKeyboard))
            toolbar.items = [flexSpace, doneButton]
            toolbar.sizeToFit()
            textField.inputAccessoryView = toolbar

            textField.text = viewModel.unformattedValue
        }
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn _: NSRange, replacementString string: String) -> Bool {
        guard !string.isEmpty else {
            return true
        }

        guard textField == valueTextField,
            let text = textField.text, !text.isEmpty
        else {
            return true
        }

        let number = numberFormatter.number(from: text + string)
        return number != nil
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == nameTextField {
            valueTextField.becomeFirstResponder()
        } else if textField == valueTextField {
            textField.resignFirstResponder()
        }

        return true
    }
}
