//
//  ItemsHeaderView.swift
//  Budget
//
//  Created by Camilo Gaviria on 21/01/2020.
//  Copyright © 2020 heima. All rights reserved.
//

import UIKit

protocol ItemsHeaderViewDelegate: AnyObject {
    func itemsHeaderView(_ itemsHeaderView: ItemsHeaderView, didChangeSortBy index: Int)
}

class ItemsHeaderView: UITableViewHeaderFooterView {
    @IBOutlet private var segmentedControl: UISegmentedControl!
    @IBOutlet private var totalValueLabel: UILabel!
    @IBOutlet private var totalActiveValueLabel: UILabel!

    weak var delegate: ItemsHeaderViewDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()

        segmentedControl.removeAllSegments()
        for (index, sortBy) in SortBy.allValues.enumerated() {
            segmentedControl.insertSegment(withTitle: sortBy.description, at: index, animated: false)
        }

        segmentedControl.selectedSegmentIndex = 0
    }

    func update(with total: String?, activeTotal: String?, sortBy: SortBy) {
        totalValueLabel.text = total
        totalActiveValueLabel.text = activeTotal

        let selectedIndex = SortBy.allValues.firstIndex(of: sortBy)
        segmentedControl.selectedSegmentIndex = selectedIndex ?? 0
    }

    @IBAction private func segementedControlValueChanged(_: UISegmentedControl) {
        delegate?.itemsHeaderView(self, didChangeSortBy: segmentedControl.selectedSegmentIndex)
    }
}
