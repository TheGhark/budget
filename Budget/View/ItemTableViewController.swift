//
//  ItemTableViewController.swift
//  Budget
//
//  Created by Camilo Gaviria on 10/01/2020.
//  Copyright © 2020 heima. All rights reserved.
//

import UIKit

class ItemTableViewController: BaseItemTableViewController {
    private let detailCellIdentifier = "DetailCellIdentifier"
    private let typeCellIdentifier = "ItemTypeCellIdentifier"

    let itemViewModel = ItemViewModel()

    var item: Item {
        get { return itemViewModel.item }
        set { itemViewModel.item = newValue }
    }

    override var viewModel: BaseItemViewModel! {
        get { return itemViewModel }
        set {}
    }

    override func setupAppeareance() {
        super.setupAppeareance()
        nameTextField.text = item.name
        valueTextField.text = viewModel.formattedValue
    }

    @IBAction override func nameEditingDidEnd(_ sender: UITextField) {
        super.nameEditingDidEnd(sender)
        title = item.name
    }

    override func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
        return isDatePickerVisible ? 5 : 4
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)

        if cell?.reuseIdentifier == dateCellIdentifier {
            isDatePickerVisible = !isDatePickerVisible
            tableView.beginUpdates()

            if isDatePickerVisible {
                dateLabel.textColor = .budgetRed
                tableView.insertRows(at: [IndexPath(row: 4, section: 0)], with: .automatic)
            } else {
                dateLabel.textColor = .systemBlue
                tableView.deleteRows(at: [IndexPath(row: 4, section: 0)], with: .automatic)
            }

            tableView.endUpdates()
        }

        tableView.deselectRow(at: indexPath, animated: true)
    }
}
