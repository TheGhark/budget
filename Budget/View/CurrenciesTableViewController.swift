//
//  CurrenciesTableViewController.swift
//  Budget
//
//  Created by Camilo Gaviria on 15/01/2020.
//  Copyright © 2020 heima. All rights reserved.
//

import UIKit

class CurrenciesTableViewController: UITableViewController {
    private let cellIdentifier = "CurrencyCellIdentifier"
    private let unwindSegueIdentifier = "UnwindToNewTransferWiseItemSegueIdentifier"

    private let viewModel = CurrenciesViewModel()
    private(set) var selectedCurrency: Currency?

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        title = NSLocalizedString("Currencies", comment: "Currencies")
    }

    override func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
        return viewModel.filteredCurrencies.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let currency = viewModel.currency(at: indexPath)
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? CurrencyTableViewCell

        guard let currrencyCell = cell else {
            fatalError("Cannot dequeue cell")
        }

        currrencyCell.update(with: currency)
        return currrencyCell
    }

    override func tableView(_: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedCurrency = viewModel.currency(at: indexPath)
        performSegue(withIdentifier: unwindSegueIdentifier, sender: self)
    }

    @IBAction func cancelButtonTapped(_: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }

    @IBAction func unwindToCurrencies(_: UIStoryboardSegue) {
        viewModel.fetch()
        tableView.reloadData()
    }
}

extension CurrenciesTableViewController: UISearchBarDelegate {
    func searchBar(_: UISearchBar, textDidChange searchText: String) {
        viewModel.filter(by: searchText)
        tableView.reloadData()
    }
}
