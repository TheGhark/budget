//
//  BudgetTableViewController.swift
//  Budget
//
//  Created by Camilo Gaviria on 17/12/2019.
//  Copyright © 2019 heima. All rights reserved.
//

import UIKit

class BudgetTableViewController: UITableViewController {
    let numberFormatter = NumberFormatter()
    @IBOutlet var incomingActiveTotalLabel: UILabel!
    @IBOutlet var incomingTotalLabel: UILabel!
    @IBOutlet var outgoingTotalLabel: UILabel!
    @IBOutlet var costsLabel: UILabel!
    @IBOutlet var availableTextField: UITextField!
    @IBOutlet var groceriesLabel: UILabel!
    @IBOutlet var totalLabel: UILabel!
    @IBOutlet var netTotalLabel: UILabel!

    private let viewModel = BudgetViewModel()

    private enum SegueIdentifiers: String {
        case incoming = "IncomingSegueIdentifier"
        case outgoing = "OutgoingSegueIdentifier"
        case groceries = "GroceriesSegueIdentifier"

        static let all: [SegueIdentifiers] = [.outgoing, .incoming, .groceries]
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(handleRefreshController), for: .valueChanged)
        tableView.refreshControl = refreshControl

        availableTextField.delegate = self
        viewModel.saveHandler = { [weak self] in
            self?.tableView.reloadSections(IndexSet(integer: 2), with: .none)
        }
        title = NSLocalizedString("Budget", comment: "Budget")
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        update()
    }

    override func prepare(for segue: UIStoryboardSegue, sender _: Any?) {
        if segue.identifier == SegueIdentifiers.incoming.rawValue {
            let itemsViewController = segue.destination as? ItemsViewController
            itemsViewController?.itemType = .debit
        } else if segue.identifier == SegueIdentifiers.outgoing.rawValue {
            let itemsViewController = segue.destination as? ItemsViewController
            itemsViewController?.itemType = .credit
        } else if segue.identifier == SegueIdentifiers.groceries.rawValue {
            let groceriesViewController = segue.destination as? GroceriesTableViewController

            guard let groceries = viewModel.groceries else {
                fatalError("Cannot found groceries model")
            }

            groceriesViewController?.groceries = groceries
        }
    }

    override func tableView(_: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        let isActiveDebits = indexPath.section == 1 && indexPath.row == 0
        let isGroceries = indexPath.section == 2 && indexPath.row == 2

        if indexPath.section == 0 || isActiveDebits || isGroceries {
            return indexPath
        }

        return nil
    }

    override func tableView(_: UITableView, didSelectRowAt indexPath: IndexPath) {
        let segueIdentifier = SegueIdentifiers.all[indexPath.section]
        performSegue(withIdentifier: segueIdentifier.rawValue, sender: nil)
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 1, indexPath.row == 1 {
            netTotalLabel.text = viewModel.netTotal
        } else if indexPath.section == 2, indexPath.row == 3 {
            totalLabel.text = viewModel.total
        }

        return super.tableView(tableView, cellForRowAt: indexPath)
    }

    override func tableView(_: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return NSLocalizedString("Outgoing", comment: "Credits")
        } else if section == 1 {
            return NSLocalizedString("Incoming", comment: "Debits")
        }

        return NSLocalizedString("Budget", comment: "Budget")
    }

    @IBAction func availableEditingDidEnd(_ sender: UITextField) {
        viewModel.update(available: sender.text)
        sender.text = viewModel.formattedAvailableValue
    }

    @objc func dismissKeyboard() {
        availableTextField.resignFirstResponder()
    }

    @objc private func handleRefreshController() {
        DispatchQueue.main.async {
            self.viewModel.fetch()
            self.update()
            self.tableView.refreshControl?.endRefreshing()
            self.tableView.reloadData()
        }
    }

    private func update() {
        incomingActiveTotalLabel.text = viewModel.activeTotalDebits
        incomingTotalLabel.text = viewModel.totalDebits
        outgoingTotalLabel.text = viewModel.totalCredits
        costsLabel.text = viewModel.currentCost
        availableTextField.text = viewModel.formattedAvailableValue
        groceriesLabel.text = viewModel.totalGroceries
        totalLabel.text = viewModel.total
    }
}

extension BudgetTableViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(dismissKeyboard))
        toolbar.items = [flexSpace, doneButton]
        toolbar.sizeToFit()
        textField.inputAccessoryView = toolbar

        textField.text = viewModel.unformattedAvailableValue
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn _: NSRange, replacementString string: String) -> Bool {
        guard !string.isEmpty else {
            return true
        }

        guard let text = textField.text, !text.isEmpty else {
            return true
        }

        let number = numberFormatter.number(from: text + string)
        return number != nil
    }

    func textFieldShouldReturn(_: UITextField) -> Bool {
        return true
    }
}
