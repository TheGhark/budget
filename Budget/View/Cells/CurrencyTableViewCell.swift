//
//  CurrencyTableViewCell.swift
//  Budget
//
//  Created by Camilo Gaviria on 16/01/2020.
//  Copyright © 2020 heima. All rights reserved.
//

import UIKit

class CurrencyTableViewCell: UITableViewCell {
    @IBOutlet private var codeLabel: UILabel!
    @IBOutlet private var nameLabel: UILabel!

    func update(with currency: Currency?) {
        codeLabel.text = currency?.code
        nameLabel.text = currency?.name
    }
}
