//
//  ItemTableViewCell.swift
//  Budget
//
//  Created by Camilo Gaviria on 10/01/2020.
//  Copyright © 2020 heima. All rights reserved.
//

import UIKit

class ItemTableViewCell: UITableViewCell {
    @IBOutlet private var nameLabel: UILabel!
    @IBOutlet private var valueLabel: UILabel!
    @IBOutlet private var dueLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()

        editingAccessoryType = .disclosureIndicator
    }

    func update(with item: Item?, value: String?, formattedDay: String?) {
        nameLabel.text = item?.name
        valueLabel.text = value

        let activeColor: UIColor = item?.type == ItemType.debit.rawValue ? .budgetLightGreen : .budgetRed
        valueLabel.textColor = item?.active == true ? activeColor : UIColor.budgetLightGrey.withAlphaComponent(0.8)

        guard let formattedDay = formattedDay else {
            dueLabel.text = NSLocalizedString("Date not set", comment: "Date not set")
            return
        }

        dueLabel.text = String(format: NSLocalizedString("Due on", comment: "Pay on the %@"), formattedDay)
    }
}
