//
//  ItemTypeTableViewCell.swift
//  Budget
//
//  Created by Camilo Gaviria on 10/01/2020.
//  Copyright © 2020 heima. All rights reserved.
//

import UIKit

class ItemTypeTableViewCell: UITableViewCell {
    @IBOutlet private var segmentedControl: UISegmentedControl!

    override func awakeFromNib() {
        super.awakeFromNib()

        segmentedControl.removeAllSegments()
        for (index, type) in ItemType.allTypes.enumerated() {
            segmentedControl.insertSegment(withTitle: type.description, at: index, animated: false)
        }
    }

    func update(with type: String?) {
        switch type {
        case ItemType.credit.rawValue:
            segmentedControl.selectedSegmentIndex = 0
        default:
            segmentedControl.selectedSegmentIndex = 1
        }
    }
}
