//
//  BudgetWidgetViewController.swift
//  BudgetWidget
//
//  Created by Camilo Gaviria on 20/01/2020.
//  Copyright © 2020 heima. All rights reserved.
//

import CoreData
import NotificationCenter
import UIKit

class BudgetWidgetViewController: UITableViewController, NCWidgetProviding {
    @IBOutlet var totalLabel: UILabel!
    @IBOutlet var totalValueLabel: UILabel!
    @IBOutlet var costsLabel: UILabel!
    @IBOutlet var costsValueLabel: UILabel!

    private let viewModel = BudgetWidgetViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()

        totalLabel.text = NSLocalizedString("Available", comment: "Available")
        costsLabel.text = NSLocalizedString("Costs", comment: "Costs")
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.fetch { [unowned self] _ in
            self.totalValueLabel.text = self.viewModel.total
            self.costsValueLabel.text = self.viewModel.formattedCostsValue
        }
    }

    override func tableView(_: UITableView, didSelectRowAt _: IndexPath) {
        if let appURL = URL(string: "budgetwidget://") {
            extensionContext?.open(appURL, completionHandler: nil)
        }
    }

    func widgetPerformUpdate(completionHandler: @escaping (NCUpdateResult) -> Void) {
        viewModel.fetch {
            completionHandler($0)
        }
    }
}
