//
//  BudgetWidgetViewModel.swift
//  BudgetWidget
//
//  Created by Camilo Gaviria on 20/01/2020.
//  Copyright © 2020 heima. All rights reserved.
//

import Foundation
import NotificationCenter

class BudgetWidgetViewModel {
    typealias FetchHandler = (NCUpdateResult) -> Void

    private let currencyFormatter = NumberFormatterFactory.formatter(type: .currency)
    private let storageManager = CoreDataManager.shared.storageManager
    private var budget: Budget?

    private var costs: NSNumber {
        let costs = storageManager.activeCredits()
        return NSNumber(value: costs)
    }

    var total: String? {
        guard let budget = self.budget else {
            return nil
        }
        return currencyFormatter.string(from: NSNumber(value: budget.total))
    }

    var formattedCostsValue: String {
        guard let formattedAvailableValue = currencyFormatter.string(from: costs) else {
            return ""
        }

        return formattedAvailableValue
    }

    func fetch(completion: FetchHandler? = nil) {
        let previousTotal = budget?.total ?? 0
        budget = storageManager.fetchBudget()

        if budget == nil {
            completion?(.failed)
        } else if budget?.total != previousTotal {
            completion?(.newData)
        } else {
            completion?(.noData)
        }
    }
}
