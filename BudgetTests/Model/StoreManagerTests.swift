//
//  StoreManagerTests.swift
//  BudgetTests
//
//  Created by Camilo Gaviria on 22/01/2020.
//  Copyright © 2020 heima. All rights reserved.
//

@testable import Budget
import XCTest

class StoreManagerTests: XCTestCase {
    var sut: StorageManager!

    override func setUp() {
        super.setUp()
        sut = CoreDataHelper().storageManager
    }

    override func tearDown() {
        super.tearDown()
        sut = nil
    }

    func testInsertCurrency() {
        sut.insertCurrency(name: "Colombian Peso", code: "COP")
        sut.save()
        let currencies = sut.fetchCurrencies()
        let found = currencies.first
        XCTAssertEqual(found?.code, "COP")
    }

    func testRemove() {
        guard let item = sut.insertItem(name: "Test", value: 100, type: .credit, isActive: true, day: 5) else {
            preconditionFailure("Could not create an item")
        }

        sut.save()
        sut.remove(item: item)
        let items = sut.fetchCredits()
        XCTAssertTrue(items.isEmpty)
    }
}
