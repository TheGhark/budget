//
//  ItemTypeTests.swift
//  BudgetTests
//
//  Created by Camilo Gaviria on 22/01/2020.
//  Copyright © 2020 heima. All rights reserved.
//

@testable import Budget
import XCTest

class ItemTypeTests: XCTestCase {
    func testDescription() {
        XCTAssertEqual(ItemType.credit.description, NSLocalizedString("credit", comment: "credit"))
        XCTAssertEqual(ItemType.debit.description, NSLocalizedString("debit", comment: "debit"))
    }
}
