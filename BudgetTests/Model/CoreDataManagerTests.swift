//
//  CoreDataManagerTests.swift
//  BudgetTests
//
//  Created by Camilo Gaviria on 22/01/2020.
//  Copyright © 2020 heima. All rights reserved.
//

import Base
@testable import Budget
import XCTest

class CoreDataManagerTests: XCTestCase {
    var coreDataHelper: CoreDataHelper!
    var sut: CoreDataManager!
    var client: BorderlessAccountServiceMock!

    override func setUp() {
        super.setUp()
        coreDataHelper = CoreDataHelper()
        let configuration = Configuration(bundle: Bundle(for: type(of: self)), name: "TWConfiguration")
        let restClient = RestClient(configuration: configuration)
        let openClient = OpenExchangeRatesClientMock(client: restClient)
        client = BorderlessAccountServiceMock(client: restClient)
        sut = CoreDataManager(persistentContainer: coreDataHelper.storageManager?.persistentContainer as? BudgetPersistentContainer,
                              storageManager: coreDataHelper.storageManager,
                              borderlessAccountService: client)
    }

    override func tearDown() {
        super.tearDown()
        sut = nil
        coreDataHelper = nil
        client = nil
    }

    func testSave() {
        coreDataHelper.storageManager.insertItem(name: "Test", value: 100, type: .credit, isActive: true, day: 5)
        sut.save()
        let items = coreDataHelper.storageManager.fetchCredits()
        XCTAssertTrue(items.count == 1)
    }

    func testInitialize() {
        let expectation = XCTestExpectation(description: "Initialize Core Data")
        sut.initialize { error in
            expectation.fulfill()
            XCTAssertNil(error)
        }

        wait(for: [expectation], timeout: 5)
    }

    func testInitialize_Error() {
        let expectation = XCTestExpectation(description: "Initialize Core Data - Error")
        client.expectedError = .invalidToken

        sut.initialize { error in
            expectation.fulfill()
            XCTAssertEqual(error as? RestClientError, .invalidToken)
        }

        wait(for: [expectation], timeout: 5)
    }
}
