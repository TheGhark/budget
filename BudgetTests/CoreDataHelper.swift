//
//  BaseTestCase.swift
//  ExpensesTests
//
//  Created by Camilo Gaviria on 05/12/2019.
//  Copyright © 2019 Heima. All rights reserved.
//

@testable import Budget
import CoreData

class CoreDataHelper {
    var storageManager: StorageManager!

    lazy var managedObjectModel: NSManagedObjectModel = {
        let managedObjectModel = NSManagedObjectModel.mergedModel(from: [Bundle(for: type(of: self))])!
        return managedObjectModel
    }()

    lazy var mockPersistentContainer: BudgetPersistentContainer = {
        let container = BudgetPersistentContainer(name: "StorageManager", managedObjectModel: self.managedObjectModel)
        let description = NSPersistentStoreDescription()
        description.type = NSInMemoryStoreType
        description.shouldAddStoreAsynchronously = false

        container.persistentStoreDescriptions = [description]
        container.loadPersistentStores { description, error in
            precondition(description.type == NSInMemoryStoreType)

            if let error = error {
                fatalError("Create an in-mem coordinator failed \(error)")
            }
        }
        return container
    }()

    init() {
        storageManager = StorageManager(persistentContainer: mockPersistentContainer)
    }

    func flushData(entity: String) {
        let fetchRequest: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest<NSFetchRequestResult>(entityName: entity)

        do {
            let managedObjects = try mockPersistentContainer.viewContext.fetch(fetchRequest)
            for case let manageObject as NSManagedObject in managedObjects {
                mockPersistentContainer.viewContext.delete(manageObject)
            }
            try mockPersistentContainer.viewContext.save()
        } catch {
            print("Could not flush data: \(error)")
        }
    }
}
