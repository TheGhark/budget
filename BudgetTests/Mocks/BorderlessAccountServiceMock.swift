//
//  BorderlessAccountServiceMock.swift
//  BudgetTests
//
//  Created by Camilo Gaviria on 22/01/2020.
//  Copyright © 2020 heima. All rights reserved.
//

import Base
import BorderlessAccount
import UIKit

class BorderlessAccountServiceMock: BorderlessAccountService {
    var expectedError: RestClientError?

    override func currencies(completion: @escaping BorderlessAccountService.CurrenciesCompletionHandler) {
        if let expectedError = expectedError {
            completion(nil, expectedError)
        } else {
            let response = CurrenciesResponse(currencies: [])
            completion(response, nil)
        }
    }
}
