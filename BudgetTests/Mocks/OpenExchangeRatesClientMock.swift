//
//  OpenExchangeRatesClientMock.swift
//  BudgetTests
//
//  Created by Camilo Gaviria on 22/01/2020.
//  Copyright © 2020 heima. All rights reserved.
//

import OpenExchangeRates
import UIKit

class OpenExchangeRatesClientMock: OpenExchangeRatesService {
    override func currencies(completion: @escaping OpenExchangeRatesService.CompletionHandler) {
        completion(["USD": "American Dollar"], nil)
    }
}
