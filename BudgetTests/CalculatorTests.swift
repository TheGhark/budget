//
//  CalculatorTests.swift
//  BudgetTests
//
//  Created by Camilo Gaviria on 17/01/2020.
//  Copyright © 2020 heima. All rights reserved.
//

@testable import Budget
import XCTest

class CalculatorTests: XCTestCase {
    var sut: Calculator!
    var coreDataHelper: CoreDataHelper!

    var storageManager: StorageManager {
        return coreDataHelper.storageManager
    }

    override func setUp() {
        super.setUp()

        coreDataHelper = CoreDataHelper()
        sut = Calculator()
    }

    override func tearDown() {
        super.tearDown()
        coreDataHelper.flushData(entity: "Item")
        coreDataHelper.flushData(entity: "Budget")
        sut = nil
    }

    func testUpdateBudget_NoBudget() {
        let budget = storageManager.fetchBudget()
        XCTAssertNil(budget)
    }

    func testUpdateBudget() {
        var budget = storageManager.insertBudget()
        let groceries = budget?.groceries
        groceries?.value = 300

        sut.updateBudget(available: 200, groceries: groceries)
        budget = storageManager.fetchBudget()
        guard budget != nil else {
            preconditionFailure("No budget was created")
        }

        XCTAssertEqual(budget?.total, -100)
    }

    func testUpdateBudget_GroceriesOnly() {
        var budget = storageManager.insertBudget()
        let groceries = budget?.groceries
        groceries?.value = 300

        sut.updateBudget(groceries: groceries)
        budget = storageManager.fetchBudget()
        guard budget != nil else {
            preconditionFailure("No budget was created")
        }

        XCTAssertEqual(budget?.total, -300)
    }

    func testUpdateBudget_AvailableOnly() {
        var budget = storageManager.insertBudget()
        storageManager.save()
        budget = sut.updateBudget(available: 500)
        XCTAssertEqual(budget?.total, 500)
    }

    func testUpdateBudgetItem_NoBudget() {
        guard let item = storageManager.insertItem(
            name: "Item",
            value: 100,
            type: .credit,
            isActive: true,
            day: 5,
            context: coreDataHelper.mockPersistentContainer.viewContext
        ) else {
            preconditionFailure("An item is required")
        }

        let budget = sut.updateBudget(with: item, context: coreDataHelper.mockPersistentContainer.viewContext)
        XCTAssertNil(budget)
    }

    func testUpdateBudgetItem_ActiveCredit() {
        var budget = storageManager.insertBudget(context: coreDataHelper.mockPersistentContainer.viewContext)
        guard let item = storageManager.insertItem(
            name: "Item",
            value: 100,
            type: .credit,
            isActive: true,
            day: 5,
            context: coreDataHelper.mockPersistentContainer.viewContext
        ) else {
            preconditionFailure("An item is required")
        }

        budget = sut.updateBudget(with: item, context: coreDataHelper.mockPersistentContainer.viewContext)
        XCTAssertEqual(budget?.total, -100)
    }

    func testUpdateBudgetItem_ActiveCredit_AutoSave() {
        var budget = storageManager.insertBudget(context: coreDataHelper.mockPersistentContainer.viewContext)
        guard let item = storageManager.insertItem(
            name: "Item",
            value: 100,
            type: .credit,
            isActive: true,
            day: 5,
            context: coreDataHelper.mockPersistentContainer.viewContext
        ) else {
            preconditionFailure("An item is required")
        }

        budget = sut.updateBudget(autoSave: true, with: item, context: coreDataHelper.mockPersistentContainer.viewContext)
        XCTAssertEqual(budget?.total, -100)
    }

    func testUpdateBudgetItem_InactiveCredit() {
        var budget = storageManager.insertBudget(context: coreDataHelper.mockPersistentContainer.viewContext)
        guard let item = storageManager.insertItem(
            name: "Item",
            value: 100,
            type: .credit,
            isActive: false,
            day: 5,
            context: coreDataHelper.mockPersistentContainer.viewContext
        ) else {
            preconditionFailure("An item is required")
        }

        budget = sut.updateBudget(with: item, context: coreDataHelper.mockPersistentContainer.viewContext)
        XCTAssertEqual(budget?.total, 100)
    }

    func testUpdateBudgetItem_ActiveDebit() {
        var budget = storageManager.insertBudget(context: coreDataHelper.mockPersistentContainer.viewContext)
        guard let item = storageManager.insertItem(
            name: "Item",
            value: 100,
            type: .debit,
            isActive: true,
            day: 5,
            context: coreDataHelper.mockPersistentContainer.viewContext
        ) else {
            preconditionFailure("An item is required")
        }

        budget = sut.updateBudget(with: item, context: coreDataHelper.mockPersistentContainer.viewContext)
        XCTAssertEqual(budget?.total, 100)
    }

    func testUpdateBudgetItem_InactiveDebit() {
        var budget = storageManager.insertBudget(context: coreDataHelper.mockPersistentContainer.viewContext)
        guard let item = storageManager.insertItem(
            name: "Item",
            value: 100,
            type: .debit,
            isActive: false,
            day: 5,
            context: coreDataHelper.mockPersistentContainer.viewContext
        ) else {
            preconditionFailure("An item is required")
        }

        budget = sut.updateBudget(with: item, context: coreDataHelper.mockPersistentContainer.viewContext)
        XCTAssertEqual(budget?.total, 0)
    }
}
