//
//  UIColorTests.swift
//  BudgetTests
//
//  Created by Camilo Gaviria on 22/01/2020.
//  Copyright © 2020 heima. All rights reserved.
//

@testable import Budget
import XCTest

class UIColorTests: XCTestCase {
    func testColors() {
        var color = UIColor.budgetWhite
        XCTAssertNotNil(color)
        color = UIColor.budgetDarkGreen
        XCTAssertNotNil(color)
        color = UIColor.budgetLightGreen
        XCTAssertNotNil(color)
        color = UIColor.budgetGrey
        XCTAssertNotNil(color)
        color = UIColor.budgetLightGrey
        XCTAssertNotNil(color)
        color = UIColor.budgetRed
        XCTAssertNotNil(color)
        color = UIColor.budgetBlack
        XCTAssertNotNil(color)
        color = UIColor.budgetBlue
        XCTAssertNotNil(color)
    }
}
