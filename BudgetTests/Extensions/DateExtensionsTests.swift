//
//  DateExtensionsTests.swift
//  BudgetTests
//
//  Created by Camilo Gaviria on 22/01/2020.
//  Copyright © 2020 heima. All rights reserved.
//

@testable import Budget
import XCTest

class DateExtensionsTests: XCTestCase {
    func testInitWithDateString() {
        guard let date = Date("2020-01-22") else {
            preconditionFailure("Could not create date")
        }

        let year = Calendar.current.component(.year, from: date)
        let month = Calendar.current.component(.month, from: date)
        let day = Calendar.current.component(.day, from: date)
        XCTAssertEqual(year, 2020)
        XCTAssertEqual(month, 1)
        XCTAssertEqual(day, 22)
    }

    func testInitWithDateString_Error() {
        var date = Date("asdfgfdsasd")
        XCTAssertNil(date)
        date = Date("2020-02-30")
        XCTAssertNil(date)
    }
}
