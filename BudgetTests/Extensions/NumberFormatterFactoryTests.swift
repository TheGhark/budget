//
//  NumberFormatterFactoryTests.swift
//  BudgetTests
//
//  Created by Camilo Gaviria on 22/01/2020.
//  Copyright © 2020 heima. All rights reserved.
//

@testable import Budget
import XCTest

class NumberFormatterFactoryTests: XCTestCase {
    func testFactory() {
        var formatter = NumberFormatterFactory.formatter(type: .currency)
        XCTAssertEqual(formatter.currencySymbol, Locale.current.currencySymbol)
        XCTAssertEqual(formatter.numberStyle, .currency)

        formatter = NumberFormatterFactory.formatter(type: .ordinal)
        XCTAssertEqual(formatter.numberStyle, .ordinal)
    }
}
