//
//  GroceriesViewModelTests.swift
//  BudgetTests
//
//  Created by Camilo Gaviria on 17/12/2019.
//  Copyright © 2019 heima. All rights reserved.
//

@testable import Budget
import XCTest

class GroceriesViewModelTests: XCTestCase {
    var sut: GroceriesViewModel!
    var coreDataHelper: CoreDataHelper!

    var storageManager: StorageManager {
        return coreDataHelper.storageManager
    }

    override func setUp() {
        super.setUp()
        let start = Date("2020-01-01")
        sut = GroceriesViewModel(start: start)
        coreDataHelper = CoreDataHelper()
    }

    override func tearDown() {
        super.tearDown()
        sut = nil
    }

    func testNumberOfSaturdays() {
        let expected = 4
        let numberOfSaturdays = sut.numberOfSaturdays
        XCTAssertEqual(expected, numberOfSaturdays)
    }

    func testNumberOfSaturdays_Start_End() {
        sut = GroceriesViewModel(start: Date("2020-02-01"), end: Date("2020-02-28"))
        let expected = 4
        let numberOfSaturdays = sut.numberOfSaturdays
        XCTAssertEqual(expected, numberOfSaturdays)
    }

    func testTotal_Zero() {
        let budget = storageManager.insertBudget()
        guard let groceries = budget?.groceries else {
            preconditionFailure("No groceries found")
        }

        sut.groceries = groceries
        XCTAssertEqual(sut.total, 0)
    }

    func testTotal_Real() {
        let budget = storageManager.insertBudget()
        guard let groceries = budget?.groceries else {
            preconditionFailure("No groceries found")
        }

        groceries.value = 300
        groceries.count = 4
        sut.groceries = groceries

        XCTAssertEqual(sut.total, 300 * 4)
    }
}
